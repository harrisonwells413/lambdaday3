package com.nijjwal.day3.project1;

public class Test1 {

	public static void main(String... args) {

		// Lambda Expression that implements run method from Runnable interface.
		Runnable r = () -> {

			for (int i = 0; i < 1000; i++) {
				System.out.println("Child Thread");
			}

			System.out.println("End of Child Thread!");

		};

		Thread t = new Thread(r);
		t.start();

		for (int i = 0; i < 1000; i++) {
			System.out.println("Main Thread");
		}

		System.out.println("End of Main Thread!");

	}

}
