package com.nijjwal.day3.project2;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Test2 {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>();

		list.add(6);
		list.add(2);
		list.add(3);
		list.add(0);

		System.out.println("Values Before using comparator: " + list);

		// Lambda Expression to sort integer values using Comparator Functional
		// Interface.
		Comparator<Integer> comp = (i1, i2) -> {
			if (i1 > i2) {
				return 1;
			} else if (i1 < i2) {
				return -1;
			} else {
				return 0;
			}
		};

		Collections.sort(list, comp);
		System.out.println("Values After using comparator: " + list);

	}
}
